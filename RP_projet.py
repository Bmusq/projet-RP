import numpy as np
import time
class Tache():
    def __init__(self,id:int,len:float,prediction:float) -> None:
        self.id=id #Identifiant, faire attention à ce qu'elle soit unique
        self.len=len #Durée réelle de la tache
        self.prediction=prediction #La prédiction de la durée pour la méthonde PREDICTION
        self.progress=0.0 #L'avancée de la tache, progress<=len
        self.dateFinExec=None #date de fin d'execution de la tache
    def __eq__(self, o) -> bool:
        return self.id==o.id
    def __hash__(self) -> int:
        return self.id
    def __repr__(self) -> str:
        return f"Tâche {self.id} : durée ~{round(self.len,1)}s"#, progression {self.progress}/{self.len}"
    def getID(self)->int:
        return self.id
    def getLen(self)->float: 
        return self.len
    def setProgress(self,progress:float)->None: 
        self.progress=progress
    def getProgress(self)->float:
        return self.progress
    def getProgressRestant(self)->float:
        return self.len-self.progress
    def addProgress(self,progress:float)->None:
        if(self.progress+progress>=self.len):
            self.progress=self.len
        else:
            self.progress+=progress
    def isDone(self)->bool:
        return self.progress==self.len
    def getDateFinExec(self)->float or None:
        return self.dateFinExec
    def setDateFinExec(self,dateFinExec)->None:
        self.dateFinExec=dateFinExec
    def getPrediction(self)->float:
        return self.prediction
    def getErreurPrediction(self)->float:
        return abs(self.prediction-self.len)
    def copy(self):#sert à copier la tache courante
        tache=Tache(self.id,self.len,self.prediction)
        tache.setProgress(self.getProgress())
        tache.setDateFinExec(self.dateFinExec)
        return tache

def ROUND_ROBIN(taches:list,lambda_=1)-> list:
    """Algorithme qui applique la politique de round-robin aux taches données
    Il trouve et met dans les taches leurs date de fin d'executions
    Parameters
    ----------
    taches : list
        La liste de tâche
    Returns
    -------
    list
        La liste de tâche, avec dans chaque tâche, sa date de fin d'execution
    """    
    tachesNonFini=[t for t in taches if t.isDone()==False]#les taches non fini
    nbTacheNonFini=nbTachesRestante(taches)#on calcule le nombre de taches qui restent non fini
    nbPasDeTemps=0
    while(nbTacheNonFini!=0):
        toAdd=min([lambda_/nbTacheNonFini]+[tache.getProgressRestant() for tache in tachesNonFini])#On ajoute seulement ce qu'on peut rajouter sans terminer une tache, si on termine une tache, on doit rajouter la même valeur aux autres pour garder le nbPasDeTemps précis
        if(toAdd==lambda_/nbTacheNonFini):
            nbPasDeTemps+=1
        else:
            nbPasDeTemps+=toAdd*nbTacheNonFini
        for tache in tachesNonFini:#On fait avancer les taches de ce qu'on peut
            tache.addProgress(toAdd)
            if(tache.isDone()):#si une tâche est fini on le marque
                tache.setDateFinExec(nbPasDeTemps)
        tachesNonFini=[t for t in taches if t.isDone()==False]#les taches non fini
        nbTacheNonFini=nbTachesRestante(taches)#on calcule le nombre de taches qui restent non fini
    return taches

def SPT(taches:list)-> dict:
    """Algorithme qui renvoi un ordonnement des tâches données, il connait la durée exacte des tâches et renvoi un ordonnancement optimal
    Parameters
    ----------
    taches : list de tache
        La liste de tâches à ordonnancer
    Returns
    -------
    dict
        Un dictionnaire avec key : Place d'ordonnancement avec 0 le premier, len(taches)-1 le dernier et value : l'objet tâche
    """
    ordonnancement=dict()
    taches.sort(key=lambda tache:tache.getLen())
    for i in range(len(taches)):
        ordonnancement[i]=taches[i]
    return ordonnancement
def PREDICTION(taches:list)->dict:
    """Algorithme qui renvoi un ordonnement des tâches données, il connait la durée prédite des tâches
    Parameters
    ----------
    taches : list de tache
        La liste de tâches à ordonnancer
    Returns
    -------
    dict
        Un dictionnaire avec key : Place d'ordonnancement avec 0 le premier, len(taches)-1 le dernier et value : l'objet tâche
    """
    ordonnancement=dict()
    taches.sort(key=lambda tache:tache.getPrediction())
    for i in range(len(taches)):
        ordonnancement[i]=taches[i]
    return ordonnancement

def PREDICTION_ROUND_ROBIN(taches:list,lambda_=0.999999)->list:
    """Execute indépendemment PREDICTION et ROUND-ROBIN sur taches, les dates de fin d'executions sont le minimum des dates pour chaque tache

    Parameters
    ----------
    taches : list
        la liste de Tache
    lambda_ : float
        Valeur entre 0 et 1 vitesse de ROUND-ROBIN, avec PREDICTION de vitesse 1-lambda_

    Returns
    -------
    list
        La liste des taches avec leurs date de fin d'execution rentrée
    """    
    ordonnancement=PREDICTION(taches) #ne modifie pas taches, on peut le donner directement
    tachesAvecDateFin=ROUND_ROBIN([tache.copy() for tache in taches],lambda_=lambda_)#modifie les taches, donc on donne une copy
    dureeTacheRR=dict()
    dureeTachePRED=dict()

    for tache in tachesAvecDateFin:
        dureeTacheRR[tache]=tache.getDateFinExec()

    place=list(ordonnancement.keys())
    tachesPRED=list(ordonnancement.values())
    for i in range(len(tachesPRED)):
        dureeTachePRED[tachesPRED[i]]=sum([tachesPRED[k].getLen() for k in range(place[i])])/(1-lambda_)
        

    for tache in taches:
        tache.setDateFinExec(min(dureeTachePRED[tache],dureeTacheRR[tache]))
    return taches

def nbTachesRestante(taches:list)->int:
    """Fonction qui renvoi le nombre de tâches qui n'ont pas fini leurs executions
    Parameters
    ----------
    taches : list
        la liste de tâche à regardedr
    Returns
    -------
    int
        le nombre de tâche qui n'ont pas fini leurs executions
    """    
    nbTacheRestante=0
    for tache in taches:
        nbTacheRestante+=1 if not tache.isDone() else 0
    return nbTacheRestante
def createRandomTaches(n:int)->list:
    """Créee n tâches de durées aléatoires (les durées sont normalisés pour que la plus petite durée soit 1)
    Parameters
    ----------
    n : int
        Nombre de tâches à générer
    Returns
    -------
    list
        la liste des tâches
    """
    taches=[]
    lens=np.random.random_sample(n) #durées
    predictions=[len+np.random.rand() for len in lens] #prédictions
    mini=min(lens)
    for i in range(len(lens)): #normalisation
        lens[i]/=mini
    for i in range(n):
        taches.append(Tache(i,lens[i],predictions[i]))
    return taches
    
def valeurFonctionObj(taches:list)->float:
    """Fonction qui calcule la valeur de la fonction objectif quand toutes les tâches connaissent leur date 
    de fin d'execution
    Surtout utilisé après Round-Robin
    Parameters
    ----------
    taches : list
        la liste de tâche (chaque tache connais sa date de fin d'execution)
    Returns
    -------
    float
        La valeur de la fonction objectif
    """    
    return sum([tache.getDateFinExec() for tache in taches])
def valeurFonctionObjSelonOrdonnancement(ordonnancement:dict,lambda_=1)->float:
    """Fonction qui retourne la valeur de la fonction objectif en connaissant l'ordonnancement des taches,
    Il renvoit donc la somme des date de fin d'execution en faisant executer les tâches les unes à la suite des autres
    selon l'ordonnancement.
    Surtout utilisé après prediction ou spt
    Parameters
    ----------
    ordonnancement : dict
        dictionnaire contenant l'ordonnancement, key= place de la tache dans l'ordonnancement, value: tache
    Returns
    -------
    float
        La valeur de la fonction objectif
    """    
    valeurFonctionObj=0
    taches=[*ordonnancement.values()]
    for i in range(len(taches)):
        valeurFonctionObj+=sum([taches[k].getLen() for k in range(i)])/lambda_
    return valeurFonctionObj

def prettyPrint(taches:list,lambda_=0.5)->None:
    """Permet de print dans la console les ordonnancements trouvés ainsi que les valeurs de la fonction objectif

    Parameters
    ----------
    func : function
        L'algorithme à comparer à SPT
    taches : list
        la liste des taches à ordonnancer
    """    
    print(f"L'ordonnancement des {len(taches)} tâches trouvé \n")
    func=PREDICTION
    times=dict()
    debut=time.time()
    ordonnancement=func(taches)
    fin=time.time()
    times[func.__name__]=fin-debut
    debut=time.time()
    ordonnancement_optimal=SPT(taches)
    fin=time.time()
    times[SPT.__name__]=fin-debut

    print(f"{func.__name__} (~{round(times[func.__name__],10)}s)    |      OPTIMAL (~{round(times[SPT.__name__],10)}s)")
    nbErrors=0
    for i in range(len(ordonnancement)):
        if ordonnancement[i]!=ordonnancement_optimal[i]:
            nbErrors+=1
            err=f"<- Erreur tâche {ordonnancement[i].getID()} au lieu de tâche {ordonnancement_optimal[i].getID()}"
        else:
            err=""
        print(f'{ordonnancement[i]}         |     {ordonnancement_optimal[i]} {err}')
    print(f"\nValeur de la fonction objectif selon les ordonnancements :")
    tachesRR=ROUND_ROBIN(copyTaches(taches))
    tachesPREDRR=PREDICTION_ROUND_ROBIN(copyTaches(taches),lambda_)
    print(f"OPTIMAL (SPT) : ~{round(valeurFonctionObjSelonOrdonnancement(ordonnancement_optimal),3)} | {func.__name__} : ~{round(valeurFonctionObjSelonOrdonnancement(ordonnancement),3)} | ROUND-ROBIN : ~{round(valeurFonctionObj(tachesRR),3)} | PREDICTION-ROUND-ROBIN: ~{round(valeurFonctionObj(tachesPREDRR),3)} avec lambda={lambda_}")


def myExemple(func,lambda_=0.5):
    """Une fonction qui test la fonction func un exemple simple et imprime les résultats
    """
    print(f"Fonction {func.__name__}, valeur lambda : {lambda_}")
    taches=[]
    taches.append(Tache(0,3/2,1))
    taches.append(Tache(1,1,1))
    taches.append(Tache(2,3,1))
    taches.append(Tache(3,2,1))
    if(func==ROUND_ROBIN):
        tachesRR=ROUND_ROBIN(copyTaches(taches))
        for tache in tachesRR:
            print(f"{tache} fini à {tache.getDateFinExec()}")
        print(f"Valeur la fonction objectif {valeurFonctionObj(tachesRR)}")
    elif(func==PREDICTION):
        ordonnancementPRED=PREDICTION(taches)
        for place,tache in ordonnancementPRED.items():
            print(f"{tache} ordonné à la {place}ème/ère place")
        print(f"Valeur la fonction objectif {valeurFonctionObjSelonOrdonnancement(ordonnancementPRED)}")
    elif(func==SPT):
        ordonnancementSPT=SPT(taches)
        for place,tache in ordonnancementSPT.items():
            print(f"{tache}, ordonné à la {place}{'eme' if place!=1 else 'ere'} place")
        print(f"Valeur la fonction objectif {valeurFonctionObjSelonOrdonnancement(ordonnancementSPT)}")
    elif(func==PREDICTION_ROUND_ROBIN):
        tachesPREDRR=PREDICTION_ROUND_ROBIN(copyTaches(taches),lambda_)
        for tache in tachesPREDRR:
            print(f"{tache} fini à {tache.getDateFinExec()}")
        print(f"Valeur la fonction objectif {valeurFonctionObj(tachesPREDRR)}")


def copyTaches(taches:list)->list:
    """retourne une copie de la liste de tache
    Parameters
    ----------
    taches : list
        la liste de taches à copier
    Returns
    -------
    list
        Une nouvelle instance de la liste de tache
    """    
    return [tache.copy() for tache in taches]
taches=createRandomTaches(10)
prettyPrint(taches,lambda_=0.5)
# myExemple(PREDICTION_ROUND_ROBIN,0.7)
# myExemple(ROUND_ROBIN,0.7)
# myExemple(PREDICTION,0.7)
# myExemple(SPT,0.7)
